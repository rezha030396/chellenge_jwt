const db = require("../config/db.js");
const Book = db.book;
const asyncMiddleware = require("express-async-handler");


exports.addbook = asyncMiddleware(async (req, res) => {
  // Save book to Database
  const book = await Book.create({
    title: req.body.title,
    author: req.body.author,
    published_date: req.body.published_date,
    pages: req.body.pages,
    language: req.body.language,
    publisher_id: req.body.publisher_id
  });

  res.status(201).send({
    status: "insert successfully!",
    data:book
  });
});

exports.viewBook = asyncMiddleware(async (req, res) => {
    // Save User to Database
  
  
    const book = await Book.findAll({})
  
    res.status(201).send({
      status: "book found!",
      data:book
    });
  });

  exports.viewBookId = asyncMiddleware(async (req, res) => {
    // Save User to Database
  
  
    const book = await Book.findOne({
        where:{
            id:req.params.id
        }
    })
  
    res.status(201).send({
      status: "book found!",
      data:book
    });
  });

  exports.updateBook = asyncMiddleware(async (req, res) => {
    // Save book to Database
    const book = await Book.update({
      title: req.body.title,
      author: req.body.author,
      published_date: req.body.published_date,
      pages: req.body.pages,
      language: req.body.language,
      publisher_id: req.body.publisher_id
    },{
        where:{
            id:req.params.id
        }
    });
  
    res.status(201).send({
      status: "insert successfully!",
      data:book
    });
  });
//hapus buku by id
  exports.deletebook = asyncMiddleware(async (req, res) => {
      const id_book = req.params.id;
    const book = await Book.destroy({
     where:{
         id:id_book
     }
    });
  
    res.status(201).send({
      status: "delete successfully!",
      data:book
    });
  });