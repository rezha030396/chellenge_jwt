const db = require("../config/db.js");
const Order = db.order;
const asyncMiddleware = require("express-async-handler");


exports.addorder = asyncMiddleware(async (req, res) => {
  // Save book to Database
  const order = await Order.create({
    bookId:req.body.bookId,
    userId:req.body.userId
  });

  res.status(201).send({
    status: "insert successfully!",
    data:order
  });
});

exports.vieworder = asyncMiddleware(async (req, res) => {
    // Save User to Database
  
  
    const order = await Order.findAll({})
  
    res.status(201).send({
      status: "order found!",
      data:order
    });
  });

  exports.vieworderuserId = asyncMiddleware(async (req, res) => {
    // Save User to Database
  
  
    const order = await Order.findOne({
        where:{
            userId:req.params.userid
        }
    })
  
    res.status(201).send({
      status: "order found!",
      data:order
    });
  });

  