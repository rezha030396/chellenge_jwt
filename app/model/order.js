'use strict';
module.exports = (sequelize, DataTypes) => {
  const Order = sequelize.define('orders', {
    id:{
        type: DataTypes.INTEGER,
        primaryKey: true ,
        autoIncrement: true
    }
  });
  return Order;
};