'use strict';
module.exports = (sequelize, DataTypes) => {
  const Book = sequelize.define('books', {
    id:{
      type: DataTypes.INTEGER,
        primaryKey: true ,
        autoIncrement: true
    },
    title: DataTypes.STRING,
    author: DataTypes.STRING,
    published_date: DataTypes.DATEONLY,
    pages: DataTypes.INTEGER,
    language: DataTypes.STRING,
    publisher_id: DataTypes.STRING
  });
  return Book;
};